//
//  NameTableViewController.swift
//  LoginApp
//
//  Created by Dane Wikstrom on 10/27/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import Parse


class NameTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var currentUser = PFUser.current()
    var user: PFObject = PFObject(className:"Name")
    var allNames: Array<PFObject> = Array<PFObject>()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get all names
        let query = PFQuery(className:"Name")
                query.findObjectsInBackground { (objects, error) in
                    if error == nil {
                        // The find succeeded.
                        print("Successfully retrieved \(objects!.count) usernames.")
                        // Do something with the found objects
                        if let objects = objects {
                            for object in objects {
                                self.allNames.append(object)
                                print(object.objectId!)
                            }
                            print(self.allNames)
                        }
                    } else {
                        // Log details of the failure
                        print("Error: \(error!) \(error!._userInfo ?? "" as AnyObject)")
                    }
                    self.tableView.reloadData()
                }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "Add New Name", sender: self)
    }
    
    @IBAction func logoutButtonTapped(_ sender: UIBarButtonItem) {
        //Logout
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String! as Any )
        }
        performSegue(withIdentifier: "Logout", sender: self)
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(allNames.count)
        return allNames.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "Name Cell", for: indexPath)
     let name = allNames[indexPath.row]
     cell.textLabel?.text = (name).object(forKey: "name") as? String
     
     return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        user = allNames[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)//deselect row
    }
    
    //swift left to delete contact
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.delete
        {
            let deleteName = allNames[indexPath.row]
            
            //delete record
            let query = PFQuery(className: "Name")
            query.whereKey("name", equalTo: deleteName["name"])
            query.findObjectsInBackground { (objects, error) in
                for object in objects! {
                    object.deleteEventually()
                }
            }
            //delete from table view
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
 
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        tableView.reloadData()
    }
}
