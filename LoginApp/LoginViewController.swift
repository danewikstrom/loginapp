//
//  LoginViewController.swift
//  LoginApp
//
//  Created by Dane Wikstrom on 10/27/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailLoginTextField: UITextField!
    @IBOutlet weak var passwordLoginTextField: UITextField!
    var currentUser = PFUser.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailLoginTextField.delegate = self
        passwordLoginTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        PFUser.logInWithUsername(inBackground: emailLoginTextField.text!, password: passwordLoginTextField.text!, block: { (user, error) in
            if(user != nil){
                print(user!["email"])
            }
        })
        
        if (PFUser.current() != nil) {
            print(PFUser.current()?["email"] as? String! as Any )
        }
        
    }
    

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! NameTableViewController
        vc.currentUser = currentUser
    }


}
