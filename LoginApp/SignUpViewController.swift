//
//  SignUpViewController.swift
//  LoginApp
//
//  Created by Dane Wikstrom on 10/27/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var eMailSignUpTextField: UITextField!
    @IBOutlet weak var passwordSignUpTextField: UITextField!
    
    var currentUser = PFUser.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signMeUpButtonTapped(_ sender: Any) {
        //Sign UP
        let user = PFUser()
        user.password = passwordSignUpTextField.text!
        user.username = eMailSignUpTextField.text!
        user.email = eMailSignUpTextField.text!
        
        // other fields can be set just like with PFObject
        user.signUpInBackground { (succeeded, error) in
            if error != nil {
                print(error ?? NSString())
            } else {
                self.currentUser = user
                print("hooray")
            }
        }
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nvc = segue.destination as! NameTableViewController
        nvc.currentUser = currentUser
    }

}
