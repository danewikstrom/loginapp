//
//  ViewController.swift
//  LoginApp
//
//  Created by Dane Wikstrom on 10/27/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import Parse 

class ViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

