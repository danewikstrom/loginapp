//
//  AddNameViewController.swift
//  LoginApp
//
//  Created by Dane Wikstrom on 10/27/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import Parse

class AddNameViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    var currentUser = PFUser.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        nameTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        //Save name
        let name = PFObject(className:"Name")
                name["name"] = nameTextField.text
                name.saveInBackground {
                    (success: Bool, error: Error?) in
                    if (success) {
                        // The object has been saved.
                        print(success)
                    } else {
                        // There was a problem, check error.description
                    }
                }
        performSegue(withIdentifier: "SaveName", sender: self)
    }
}
